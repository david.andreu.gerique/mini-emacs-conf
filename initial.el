(require 'package)
 (add-to-list 'package-archives
	       '("melpa" . "https://melpa.org/packages/"))

(add-to-list 'package-archives '("ublt" . "https://elpa.ubolonton.org/packages/"))

 ;; ;; Initialise the packages, avoiding a re-initialisation.
  (unless (bound-and-true-p package--initialized)
     (setq package-enable-at-startup nil)
     (package-initialize))

  ;; Make sure `use-package' is available.
  (unless (package-installed-p 'use-package)
    (package-refresh-contents)
    (package-install 'use-package))

 ;; ;; Configure `use-package' prior to loading it.

  (eval-and-compile
   ;; The following is VERY IMPORTANT.  Write hooks using their real name
   ;; instead of a shorter version: after-init ==> `after-init-hook'.
   ;;
   ;; This is to empower help commands with their contextual awareness,
    ;; such as `describe-symbol'.
    )


(setq auto-save-default nil
      create-lockfiles nil
      make-backup-files nil)

(use-package helm
  :ensure t
  :init (progn
	   (setq helm-ff-skip-boring-files t)
	   (global-set-key (kbd "C-SPC") 'helm-M-x)
	   (global-set-key (kbd "C-x C-b") 'helm-for-files)
	   (global-set-key (kbd "C-x b") 'helm-for-files)
	   (global-set-key (kbd "C-x C-f") 'helm-find-files)
	   (helm-mode)
	)
)
(add-to-list 'display-buffer-alist
		    `(,(rx bos "*helm" (* not-newline) "*" eos)
			 (display-buffer-in-side-window)
			 (inhibit-same-window . t)
			 (window-height . 0.3)))

;; Initialize evil mode
(setq evil-want-keybinding nil)

(use-package evil
  :ensure t
  :init (evil-mode))

(use-package evil-collection
  :after evil
  :ensure t
  :config
  (evil-collection-init))

;; Main theme
(use-package modus-themes
  :ensure t)

;; Doom modeline
(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1))

;; All the icons
(use-package all-the-icons
  :ensure t)

;; Remove ugly things
(desktop-save-mode -1)
(tool-bar-mode -1)
(toggle-scroll-bar -1)

;; Fancy ^^
(column-number-mode 1)

(use-package helpful
  :ensure t
  :bind (("C-h f" . helpful-callable)
	 ("C-h v" . helpful-variable)
	 ("C-h k" . helpful-key)
	 ("C-c C-d" . helpful-at-point)
	 )
  )
;; Add evil to help buffer
(add-to-list 'evil-emacs-state-modes 'helpful-mode)

(use-package hydra
  :ensure t)

(use-package major-mode-hydra
  :ensure t
  :bind ("C-c h" . major-mode-hydra))

(use-package projectile
	     :ensure t
	     :init (projectile-mode))

(use-package treemacs
  :ensure t
  :bind (("C-x C-l". treemacs)))

(major-mode-hydra-define (treemacs-mode) 
	 (:title "Treemacs Mode" :color blue :separator "=" :exit t)
	 ("Files/Directories"
	  (("f" treemacs-create-file "create file")
	   ("d" treemacs-create-dir "create directory")
	   ("x" treemacs-delete "delete file/dir")
	   ("r" treemacs-rename "rename"))
	  "Projects"
	  (("a" treemacs-add-project "Add project")
	   ("X" treemacs-remove-project-from-workspace "Remove project"))
	  ))

(use-package dashboard
  :ensure t
  :config
  (require 'org)
  (message "%s" org-agenda-files)
  ;; Custom banner
  (setq dashboard-startup-banner "/home/david/.emacs.d/spectre.png")
  ;; Icons
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-file-icons t)
  ;; Items in dashboard
  (setq dashboard-items '((recents . 5)
			  (projects . 5)
			 ))
  ;; Centered stuff
  (setq show-week-agenda-p t)
  (setq dashboard-center-content t)
  ;; Navigator
  (setq dashboard-set-navigator t)
  ;; Custom inside jokes in footer messages
  (setq dashboard-footer-messages '("ナンナノコノキ!"))
  ;; Leaf icon
  (setq dashboard-footer-icon (all-the-icons-faicon "leaf"
						    :height 1.1
						    :v-adjust -0.05
						    :face 'font-lock-keyword-face))
  (dashboard-setup-startup-hook))
;; Use evil on dashboard
(add-to-list 'evil-emacs-state-modes 'dashboard-mode)

(use-package yasnippet
  :ensure t
  :config (yas-global-mode 1))
;; Add snippets
(use-package yasnippet-snippets
  :after yasnippets
  :ensure t)

(use-package company
    :ensure t
    :config (setq
	     company-idle-delay 0.3
	     company-tooltip-limit 24
	     company-tooltip-align-annotations t)
    :init (global-company-mode))

(use-package smartparens
  :ensure t
  :init (smartparens-global-mode))
(show-paren-mode)

(use-package helm-swoop
  :ensure t)
(define-key evil-motion-state-map (kbd "/") 'helm-swoop)

(use-package smooth-scrolling
  :ensure t
  :init (smooth-scrolling-mode 1))

;; Cool symbols instead of *
(use-package org-bullets
  :ensure t
  :after org
  :init (org-bullets-mode))

(add-hook 'org-mode-hook #'org-bullets-mode)

;; Export with boostrap
(use-package ox-twbs
  :ensure t
  :after org-mode)

(setq org-agenda-files '("~/Documents/org/"))

;; Shorcuts
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c c") 'org-capture)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((dot . t)
   (ditaa . t)))
(setq org-ditaa-jar-path "/usr/bin/ditaa")

(setq org-latex-listings 'minted
      org-latex-packages-alist '(("" "minted"))
      org-latex-pdf-process
      '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
	"pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
	"pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))


;; (add-to-list 'load-path (locate-user-emacs-file (expand-file-name "graph.el" "external_packages" )))
;; (require 'graph)

(use-package magit
  :ensure t
  :init (progn
	  (global-set-key (kbd "C-x g") 'magit-status)
	  (setq evil-magit-state 'motion)))

(setq vterm-module-cmake-args "-DUSE_SYSTEM_LIBVTERM=no")
(use-package vterm
  :ensure t)
;; Add it to evil mode
(add-to-list 'evil-emacs-state-modes 'vterm-mode)

(use-package vterm-toggle
  :ensure t
  :after vterm
  :config
  (progn
    (setq vterm-toggle-fullscreen-p nil)
    (setq vterm-toggle-scope 'project)
    (add-to-list 'display-buffer-alist
		   '((lambda(bufname _) (with-current-buffer bufname (equal major-mode 'vterm-mode)))
		     (display-buffer-reuse-window display-buffer-at-bottom)
		     (direction . bottom)
		     (dedicated . t) ;dedicated is supported in emacs27
		     (reusable-frames . visible)
		     (window-height . 0.3)))))

(global-set-key (kbd "C-x <C-return>") #'vterm-toggle)
(setq shell-file-name "bash")
(setq shell-command-switch "-ic")


(defun mbs/compile()
  "Compile and execute current .cc/.cpp file"
  (interactive)

  (let* ((file-name (buffer-file-name))
	   (exec-name (concat (file-name-base file-name) ".x")))
    (compile (format "p1++ -o %s %s" exec-name file-name))
    )
  )

(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))

(use-package flyspell
  :ensure t
  :config (add-hook 'prog-mode-hook #'flyspell-prog-mode))

(use-package tree-sitter
  :ensure t)

(use-package tree-sitter-langs
  :ensure t)

(require 'tree-sitter-langs)

(use-package undo-tree
  :ensure t
  :init (global-undo-tree-mode))

(use-package slime
:ensure t
:init (setq inferior-lisp-program "clisp"))

(use-package slime-company
:after (slime company)
:ensure t
:config (setq slime-company-completion 'fuzzy
slime-company-after-completion 'slime-company-just-one-space))
(add-hook 'lisp-mode-hook (lambda ()
		      (add-to-list 'company-backends 'company-slime)
		      (slime-setup '(slime-fancy slime-company slime-quicklisp slime-asdf))))

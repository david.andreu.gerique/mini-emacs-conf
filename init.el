(org-babel-load-file
 (expand-file-name "initial.org"
                   user-emacs-directory))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes '(modus-vivendi-deuteranopia))
 '(custom-safe-themes
   '("30dc9873c16a0efb187bb3f8687c16aae46b86ddc34881b7cae5273e56b97580" "dde643b0efb339c0de5645a2bc2e8b4176976d5298065b8e6ca45bc4ddf188b7" "250007c5ae19bcbaa80e1bf8184720efb6262adafa9746868e6b9ecd9d5fbf84" "896e4305e7c10f3217c5c0a0ef9d99240c3342414ec5bfca4ec4bff27abe2d2d" "35335d6369e911dac9d3ec12501fd64bc4458376b64f0047169d33f9d2988201" default))
 '(helm-minibuffer-history-key "M-p")
 '(org-agenda-files '("~/.emacs.d/initial.org" "~/Documents/org/"))
 '(package-selected-packages
   '(undo-tree yasnippet-snippets vterm-toggle use-package treemacs tree-sitter-langs smooth-scrolling smartparens slime-company projectile ox-twbs org-bullets modus-themes major-mode-hydra magit irony-eldoc helpful helm-swoop flycheck evil-collection doom-modeline dashboard company-irony clang-format all-the-icons)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

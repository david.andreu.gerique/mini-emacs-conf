(define-package "modus-themes" "20230228.2011" "Elegant, highly legible and customizable themes"
  '((emacs "27.1"))
  :commit "d8e92a50f003908c72a2f588beb1856041dc9cda" :authors
  '(("Protesilaos Stavrou" . "info@protesilaos.com"))
  :maintainer
  '("Modus-Themes Development" . "~protesilaos/modus-themes@lists.sr.ht")
  :keywords
  '("faces" "theme" "accessibility")
  :url "https://git.sr.ht/~protesilaos/modus-themes")
;; Local Variables:
;; no-byte-compile: t
;; End:
